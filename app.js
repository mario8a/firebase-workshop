//* HTML ELEMENTS
const todoInputTitle = document.querySelector('#todo-title');
const todoInputDesc = document.querySelector('#todo-desc');
const todoForm = document.getElementById('todo-form');

const todosContainer = document.getElementById('todos-container');

todoForm.addEventListener('submit', async (e) => {
   e.preventDefault()

   const title = todoInputTitle.value;
   const description = todoInputDesc.value;

   if (title !== '' && description !== '') {
      createTodo(title, description);
   };
   todoForm.reset();
});

window.addEventListener('DOMContentLoaded', (e) => {
   // getTodos();
   // renderTodos();
   onGetTasks();
});

const renderTodos = async () => {
  todosContainer.innerHTML = "";
   try {
      const todos = await getTodos();
      todos.forEach((todo) => {
         todosContainer.innerHTML += `
         <div class="col">
            <div class="card">
            <div class="card-body">
               <h5 class="card-title">${todo.title}</h5>
               <p class="card-text">${todo.desc}</p>
            </div>
            </div>
         </div>
         `
      });
   } catch (error) {
      throw new Error(error);
   }
}

//* Firebase
// https://firebase.google.com/docs/firestore/manage-data/add-data#web-version-8_6
const createTodo = async(title, desc) => {
   try {
      const resp = await db.collection('todos').add({
         title,
         desc
      });
      return resp;
   } catch (error) {
      throw new Error(error);
   }
}

// https://firebase.google.com/docs/firestore/query-data/queries#web-version-8_3
const getTodos = async () => {
   try {
      let itemsTodo = [];
      const resp = await db.collection("todos").get();
      resp.forEach(item => {
         itemsTodo.unshift(item.data());
      });
      // console.log(itemsTodo);
      return itemsTodo;
   } catch (error) {
      throw new Error(error);
   }
}

// https://firebase.google.com/docs/firestore/query-data/listen
const onGetTasks = async () => {
   try {
      db.collection("todos").onSnapshot(() => {
         renderTodos()
      });
   } catch (error) {
      throw new Error(error);
   }
}